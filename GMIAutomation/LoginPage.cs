﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using GMI.AcceptanceTests;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;

namespace GMI.AcceptanceTests
{
    public class LoginPage
    {
        public static void GoTo()
        {
            Driver.Instance.Navigate().GoToUrl("https://www.getmein.com/login.html");
            var wait = new WebDriverWait(Driver.Instance, TimeSpan.FromSeconds(300));
            wait.Until(d => d.SwitchTo().ActiveElement().GetAttribute("id") == "ctl00_centerContent_myAccount_txtEmailAddress");

        }

        public static LoginCommand LoginAs(string userName)
        {
            return new LoginCommand(userName);
        }

        public static LogOutCommand LogOutAs()
        {
            return new LogOutCommand();
        }
    }

    public class LoginCommand
    {
        private readonly string userName;
        private string password;


        public LoginCommand(string userName)
        {
            this.userName = userName;
        }

        public LoginCommand WithPassword(string password)
        {
            this.password = password;
            return this;
        }

        public void Login()
        {
            var loginInput = Driver.Instance.FindElement(By.Id("ctl00_centerContent_myAccount_txtEmailAddress"));
            loginInput.SendKeys(userName);

            var passwordInput = Driver.Instance.FindElement(By.Id("ctl00_centerContent_myAccount_txtPassword"));
            passwordInput.SendKeys(password);

            var loginButton = Driver.Instance.FindElement(By.Id("ctl00_centerContent_myAccount_btnSignIn"));
            Thread.Sleep(2000);
            loginButton.Click();
        }
    }

     public class LogOutCommand
    {
        public void LogOut()
        {
            var logoutLink = Driver.Instance.FindElement(By.LinkText("Log out"));
            Thread.Sleep(2000);
            logoutLink.Click();
            //Thread.Sleep(1000);
        }
    }
}
