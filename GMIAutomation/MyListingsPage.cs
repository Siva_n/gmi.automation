﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace GMI.AcceptanceTests
{
    public class MyListingsPage
    {
        public static bool IsAt
        {
            get
            {
                var h1s = Driver.Instance.FindElements(By.TagName("h1"));
                var wait = new WebDriverWait(Driver.Instance, TimeSpan.FromSeconds(1000));

                if (h1s.Count > 0)
                    return h1s[0].Text == "My Account: My Inventory";
                return false;
            }

        }

        public static bool IsLoggedOut
        {
            get
            {
                try
                {
                    var buttonExists = Driver.Instance.FindElements(By.Id("btnLogin"));
                    if (buttonExists.Count > 0)
                        if(buttonExists[0].Text == "My Account (Login)")
                            return false;
                }
                catch (NoSuchElementException exception)
                {
                    return true ;
                }

                return true;
            }

        }
    }
}
