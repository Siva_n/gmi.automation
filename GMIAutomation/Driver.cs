﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;

namespace GMI.AcceptanceTests
{
    public class Driver
    {
        public static IWebDriver Instance { get; set; }

        public static void Initialize()
        {
            Instance= new FirefoxDriver();
            Instance.Manage().Window.Maximize();
            Instance.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(1000));
        }

        public static void Close()
        {
            Instance.Close();
        }
    }
}