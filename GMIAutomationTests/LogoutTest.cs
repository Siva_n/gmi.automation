﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using GMI.AcceptanceTests;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GMITests
{
    [TestClass]
    public class LogoutTest
    {
        [TestInitialize]
        public void Init()
        {
            Driver.Initialize();
        }

        [TestMethod]
        public void User_Can_Logout()
        {
            LoginPage.GoTo();
            LoginPage.LoginAs("siva@getmein.com").WithPassword("Holidays").Login();

            Thread.Sleep(2000);
            Assert.IsTrue(MyListingsPage.IsAt, "Login Failed");
            Thread.Sleep(2000);
            LoginPage.LogOutAs().LogOut();
            
           
            var loggedout = MyListingsPage.IsLoggedOut;
            Assert.IsTrue(loggedout, "Not logged out");
            Cleanup();
        }

        private void Cleanup()
        {
            Driver.Close();
        }
    }
}
