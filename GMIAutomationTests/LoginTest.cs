﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using GMI.AcceptanceTests;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GMITests
{
    [TestClass]
    public class LoginTest
    {
        [TestInitialize]
        public void Init()
        {
            Driver.Initialize();
        }

        [TestMethod]
        public void User_Can_Login()
        {
            LoginPage.GoTo();
           
            LoginPage.LoginAs("siva@getmein.com").WithPassword("holidays").Login();
            Thread.Sleep(2000);

            Assert.IsTrue(MyListingsPage.IsAt,"Login Failed");

        }

        private void Cleanup()
        {
            Driver.Close();
        }
    }
}
